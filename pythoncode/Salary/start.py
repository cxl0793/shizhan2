from pythoncode.Salary.send_salary import send_salary
from pythoncode.Salary.show_salary import show_salary

if __name__ == '__main__':
    send_salary()
    show_salary()
