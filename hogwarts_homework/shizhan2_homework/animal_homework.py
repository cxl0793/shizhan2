# -*- coding:utf-8 -*-
# @ AUTHOR :xiaocao
# @File :animal_homework.py
'''
写一个面向对象的例子：
比如创建一个类（Animal）【动物类】，类里有属性（名称，颜色，年龄，性别），类方法（会叫，会跑）
创建子类【猫】，继承【动物类】
重写父类的__init__方法，继承父类的属性
添加一个新的属性，毛发=短毛
添加一个新的方法， 会捉老鼠，
重写父类的‘【会叫】的方法，改成【喵喵叫】
创建子类【狗】，继承【动物类】
复写父类的__init__方法，继承父类的属性
添加一个新的属性，毛发=长毛
添加一个新的方法， 会看家
复写父类的【会叫】的方法，改成【汪汪叫】
在入口函数中创建类的实例
创建一个猫猫实例
调用捉老鼠的方法
打印【猫猫的姓名，颜色，年龄，性别，毛发，捉到了老鼠】
创建一个狗狗实例
调用【会看家】的方法
打印【狗狗的姓名，颜色，年龄，性别，毛发】
'''

class Animal:

    def __init__(self,name,color,age,gender ):
        self.name = name
        self.color = color
        self.age = age
        self.gender = gender

    def saying(self):
        print('I can say')

    def running(self):
        print('I can run')

class Cat(Animal):

    def __init__(self,name,color,age, gender):
        self.hair = 'short hair'

        super().__init__(name,color,age, gender)


    def catching(self):
        print('I can catch mouse')


    def saying(self):
        print('miaomiaomiao')

class Dog(Animal):

    def __init__(self,name,color,age, gender):

        self.hair = 'long hair'

        super().__init__(name,color,age, gender)

    def watching_home(self):

        print('I can watch my house')

    def saying(self):
        print('wangwangwang')

if __name__ == '__main__':
    Cat=Cat('xiaomao','white',3,'female')
    Cat.catching()

    print(f"猫猫的姓名是{Cat.name},颜色是{Cat.color},性别是{Cat.gender},年龄是{Cat.age},毛发是{Cat.hair}")

    Dog =Dog('xiaowang','black',2,'male')
    Dog.watching_home()

    print(f"狗狗的姓名是{Cat.name},颜色是{Cat.color},性别是{Cat.gender},年龄是{Cat.age},毛发是{Cat.hair}")
