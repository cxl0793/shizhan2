# import os
#
# def get_big_file(path, filesize):
#     """
#     找出path目录下文件大小大于filesize的文件
#     :param path:
#     :param filesize:
#     :return:
#     """
#     # 遍历指定文件夹及其子文件夹
#     for dirpath, dirnames, filenames in os.walk(path):
#         for filename in filenames:
#             target_file = os.path.join(dirpath, filename)
#             # 要判断是否真的是文件,有可能是个链接哦
#             if not os.path.isfile(target_file):
#                 continue
#             size = os.path.getsize(target_file)
#             if size > filesize:
#                 size = size//(1024*1024)    # 转换兆
#                 size = '{size}M'.format(size=size)
#                 print(target_file, size)
#
# if __name__ == '__main__':
#     get_big_file('/Users/kwsy', 500*1024*1024)



import sqlite3
from urllib.parse import urlparse
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties

db_path = '/Users/zhangdongsheng/Library/Application Support/Google/Chrome/Default/History'
conn = sqlite3.connect(db_path)

sql = '''
select url from  urls where 
datetime(last_visit_time/1000000-11644473600,'unixepoch') 
> '2019-05-01'
'''
cursor = conn.execute(sql)